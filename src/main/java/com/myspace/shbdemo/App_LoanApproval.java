package com.myspace.shbdemo;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class App_LoanApproval implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("Total Credit Limit")
	private java.math.BigInteger totalCreditLimit;

	private java.lang.String approvaltype;

	private java.lang.String totalsecuredloanlimit;

	private java.lang.String totalunsecuredloanlimit;

	private java.lang.String totalcollateralvalue;

	private java.lang.String creditlimitcollateral;

	private java.lang.String loantovalueration;

	public App_LoanApproval() {
	}

	public java.math.BigInteger getTotalCreditLimit() {
		return this.totalCreditLimit;
	}

	public void setTotalCreditLimit(java.math.BigInteger totalCreditLimit) {
		this.totalCreditLimit = totalCreditLimit;
	}

	public java.lang.String getApprovaltype() {
		return this.approvaltype;
	}

	public void setApprovaltype(java.lang.String approvaltype) {
		this.approvaltype = approvaltype;
	}

	public java.lang.String getTotalsecuredloanlimit() {
		return this.totalsecuredloanlimit;
	}

	public void setTotalsecuredloanlimit(java.lang.String totalsecuredloanlimit) {
		this.totalsecuredloanlimit = totalsecuredloanlimit;
	}

	public java.lang.String getTotalunsecuredloanlimit() {
		return this.totalunsecuredloanlimit;
	}

	public void setTotalunsecuredloanlimit(
			java.lang.String totalunsecuredloanlimit) {
		this.totalunsecuredloanlimit = totalunsecuredloanlimit;
	}

	public java.lang.String getTotalcollateralvalue() {
		return this.totalcollateralvalue;
	}

	public void setTotalcollateralvalue(java.lang.String totalcollateralvalue) {
		this.totalcollateralvalue = totalcollateralvalue;
	}

	public java.lang.String getCreditlimitcollateral() {
		return this.creditlimitcollateral;
	}

	public void setCreditlimitcollateral(java.lang.String creditlimitcollateral) {
		this.creditlimitcollateral = creditlimitcollateral;
	}

	public java.lang.String getLoantovalueration() {
		return this.loantovalueration;
	}

	public void setLoantovalueration(java.lang.String loantovalueration) {
		this.loantovalueration = loantovalueration;
	}

	public App_LoanApproval(java.math.BigInteger totalCreditLimit,
			java.lang.String approvaltype,
			java.lang.String totalsecuredloanlimit,
			java.lang.String totalunsecuredloanlimit,
			java.lang.String totalcollateralvalue,
			java.lang.String creditlimitcollateral,
			java.lang.String loantovalueration) {
		this.totalCreditLimit = totalCreditLimit;
		this.approvaltype = approvaltype;
		this.totalsecuredloanlimit = totalsecuredloanlimit;
		this.totalunsecuredloanlimit = totalunsecuredloanlimit;
		this.totalcollateralvalue = totalcollateralvalue;
		this.creditlimitcollateral = creditlimitcollateral;
		this.loantovalueration = loantovalueration;
	}

}