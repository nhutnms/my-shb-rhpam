package com.myspace.shbdemo;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class App_ProposalInformation implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("Product Group")
	private java.lang.String productGroup;

	private java.lang.String product;

	private java.lang.String productdetail;

	private java.lang.String recommendation;

	private java.lang.String currentamount;

	private java.lang.String currentcurrency;

	private java.lang.String currentexchangerate;

	private java.lang.String proposedamount;

	private java.lang.String proposedcurrency;

	private java.lang.String proposedexchangerate;

	public App_ProposalInformation() {
	}

	public java.lang.String getProductGroup() {
		return this.productGroup;
	}

	public void setProductGroup(java.lang.String productGroup) {
		this.productGroup = productGroup;
	}

	public java.lang.String getProduct() {
		return this.product;
	}

	public void setProduct(java.lang.String product) {
		this.product = product;
	}

	public java.lang.String getProductdetail() {
		return this.productdetail;
	}

	public void setProductdetail(java.lang.String productdetail) {
		this.productdetail = productdetail;
	}

	public java.lang.String getRecommendation() {
		return this.recommendation;
	}

	public void setRecommendation(java.lang.String recommendation) {
		this.recommendation = recommendation;
	}

	public java.lang.String getCurrentamount() {
		return this.currentamount;
	}

	public void setCurrentamount(java.lang.String currentamount) {
		this.currentamount = currentamount;
	}

	public java.lang.String getCurrentcurrency() {
		return this.currentcurrency;
	}

	public void setCurrentcurrency(java.lang.String currentcurrency) {
		this.currentcurrency = currentcurrency;
	}

	public java.lang.String getCurrentexchangerate() {
		return this.currentexchangerate;
	}

	public void setCurrentexchangerate(java.lang.String currentexchangerate) {
		this.currentexchangerate = currentexchangerate;
	}

	public java.lang.String getProposedamount() {
		return this.proposedamount;
	}

	public void setProposedamount(java.lang.String proposedamount) {
		this.proposedamount = proposedamount;
	}

	public java.lang.String getProposedcurrency() {
		return this.proposedcurrency;
	}

	public void setProposedcurrency(java.lang.String proposedcurrency) {
		this.proposedcurrency = proposedcurrency;
	}

	public java.lang.String getProposedexchangerate() {
		return this.proposedexchangerate;
	}

	public void setProposedexchangerate(java.lang.String proposedexchangerate) {
		this.proposedexchangerate = proposedexchangerate;
	}

	public App_ProposalInformation(java.lang.String productGroup,
			java.lang.String product, java.lang.String productdetail,
			java.lang.String recommendation, java.lang.String currentamount,
			java.lang.String currentcurrency,
			java.lang.String currentexchangerate,
			java.lang.String proposedamount, java.lang.String proposedcurrency,
			java.lang.String proposedexchangerate) {
		this.productGroup = productGroup;
		this.product = product;
		this.productdetail = productdetail;
		this.recommendation = recommendation;
		this.currentamount = currentamount;
		this.currentcurrency = currentcurrency;
		this.currentexchangerate = currentexchangerate;
		this.proposedamount = proposedamount;
		this.proposedcurrency = proposedcurrency;
		this.proposedexchangerate = proposedexchangerate;
	}

}