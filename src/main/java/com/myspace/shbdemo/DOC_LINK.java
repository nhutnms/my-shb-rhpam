package com.myspace.shbdemo;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class DOC_LINK implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label(value = "file_link")
	private java.lang.String file_link;

	public DOC_LINK() {
	}

	public java.lang.String getFile_link() {
		return this.file_link;
	}

	public void setFile_link(java.lang.String file_link) {
		this.file_link = file_link;
	}

	public DOC_LINK(java.lang.String file_link) {
		this.file_link = file_link;
	}

}