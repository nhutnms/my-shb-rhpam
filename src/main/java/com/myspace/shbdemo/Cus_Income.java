package com.myspace.shbdemo;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Cus_Income implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String incomesourceID;

	private java.lang.String incomesource;

	private java.lang.String totalIncomeinVND;

	private java.lang.String income;

	private java.lang.String occupation;

	private java.lang.String incomecycle;

	private java.lang.String organizationID;

	private java.lang.String organizationname;

	public Cus_Income() {
	}

	public java.lang.String getIncomesourceID() {
		return this.incomesourceID;
	}

	public void setIncomesourceID(java.lang.String incomesourceID) {
		this.incomesourceID = incomesourceID;
	}

	public java.lang.String getIncomesource() {
		return this.incomesource;
	}

	public void setIncomesource(java.lang.String incomesource) {
		this.incomesource = incomesource;
	}

	public java.lang.String getTotalIncomeinVND() {
		return this.totalIncomeinVND;
	}

	public void setTotalIncomeinVND(java.lang.String totalIncomeinVND) {
		this.totalIncomeinVND = totalIncomeinVND;
	}

	public java.lang.String getIncome() {
		return this.income;
	}

	public void setIncome(java.lang.String income) {
		this.income = income;
	}

	public java.lang.String getOccupation() {
		return this.occupation;
	}

	public void setOccupation(java.lang.String occupation) {
		this.occupation = occupation;
	}

	public java.lang.String getIncomecycle() {
		return this.incomecycle;
	}

	public void setIncomecycle(java.lang.String incomecycle) {
		this.incomecycle = incomecycle;
	}

	public java.lang.String getOrganizationID() {
		return this.organizationID;
	}

	public void setOrganizationID(java.lang.String organizationID) {
		this.organizationID = organizationID;
	}

	public java.lang.String getOrganizationname() {
		return this.organizationname;
	}

	public void setOrganizationname(java.lang.String organizationname) {
		this.organizationname = organizationname;
	}

	public Cus_Income(java.lang.String incomesourceID,
			java.lang.String incomesource, java.lang.String totalIncomeinVND,
			java.lang.String income, java.lang.String occupation,
			java.lang.String incomecycle, java.lang.String organizationID,
			java.lang.String organizationname) {
		this.incomesourceID = incomesourceID;
		this.incomesource = incomesource;
		this.totalIncomeinVND = totalIncomeinVND;
		this.income = income;
		this.occupation = occupation;
		this.incomecycle = incomecycle;
		this.organizationID = organizationID;
		this.organizationname = organizationname;
	}

}