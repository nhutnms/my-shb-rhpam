package com.myspace.shbdemo;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class CICStatus implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("CIC Loan Group")
	private java.lang.String CICGroup;

	@org.kie.api.definition.type.Label(value = "CIC Message")
	private java.lang.String CICMessage;

	public CICStatus() {
	}

	public java.lang.String getCICGroup() {
		return this.CICGroup;
	}

	public void setCICGroup(java.lang.String CICGroup) {
		this.CICGroup = CICGroup;
	}

	public java.lang.String getCICMessage() {
		return this.CICMessage;
	}

	public void setCICMessage(java.lang.String CICMessage) {
		this.CICMessage = CICMessage;
	}

	public CICStatus(java.lang.String CICGroup, java.lang.String CICMessage) {
		this.CICGroup = CICGroup;
		this.CICMessage = CICMessage;
	}

}